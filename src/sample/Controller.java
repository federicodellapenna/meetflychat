package sample;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import javax.mail.*;
import javax.mail.search.FlagTerm;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;
import java.util.Vector;

public class Controller {
    String mit = "Federico94";//nick del mittente
    String des = "MariaLuisa95";//nick del destinatario
    @FXML
    private TextField message;//casella del messaggio
    @FXML
    private TextArea txtarea;//caselle dei messaggi
    private CheckingMails c;
    private EmailUtility e;

    @FXML
    public void handleMessage() throws Exception {
        String lastmsg = mit+": " + message.getText() + "\n";//il messaggio che verrà inviato
        message.clear();
        txtarea.appendText(lastmsg);
        //String msg=c.getMsg();
        e.sendEmail("smtp.libero.it", "25", "meetfly2019@libero.it", "noreplymeetfly",
                "meetfly2019@libero.it", "MTFMSG:" + mit + "-" + des, lastmsg);

    }

    public void mailCheck() throws Exception {

        Session session = Session.getDefaultInstance(new Properties());
        Store store = session.getStore("pop3s");
        store.connect("pop3.libero.it", 995, "meetfly2019@libero.it", "noreplymeetfly");
        Folder inbox = store.getFolder("INBOX");
        inbox.open(Folder.READ_WRITE);
        // Fetch unseen messages from inbox folder
        Message[] messages = inbox.search(new FlagTerm(new Flags(Flags.Flag.DELETED), false));
        for (Message message : messages) {
            if (message.getSubject().contains("MTFMSG:" + des + "-" + mit)) {
                System.out.println(message.getContent());
                String msg=(String) message.getContent();
                txtarea.appendText(msg);
                message.setFlag(Flags.Flag.DELETED, true);
            }
        }

        inbox.close(true);
        store.close();
        
    }

    /*@FXML
    public void handleLast() throws MessagingException, IOException {
        Session session = Session.getDefaultInstance(new Properties());
        Store store2 = session.getStore("pop3s");
        store2.connect("pop3.libero.it", 995, "meetfly2019@libero.it", "noreplymeetfly");
        Folder inbox2 = store2.getFolder("INBOX");
        inbox2.open(Folder.READ_WRITE);
        // Fetch unseen messages from inbox folder
        Message[] nMsg = inbox2.search(new FlagTerm(new Flags(Flags.Flag.DELETED), false));
        for (Message a : nMsg) {
            if (a.getSubject().contains("MTFMSG:" + des + "-" + mit)) {
                System.out.println(a.getContent());
                String msgnew=(String) a.getContent();
                txtarea.appendText(msgnew);
            }
        }
    }

     */
}




