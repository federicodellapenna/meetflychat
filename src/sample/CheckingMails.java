package sample;

import java.util.Arrays;
import java.util.Properties;

import javax.mail.*;
import javax.mail.search.FlagTerm;

public class CheckingMails {
    private String lastMsg;
    public String getMsg(){
        return lastMsg;
    }

    public void mailCheck() throws Exception {

        Session session = Session.getDefaultInstance(new Properties( ));
        Store store = session.getStore("imaps");
        store.connect("imapmail.libero.it", 993, "meetfly2019@libero.it", "noreplymeetfly");
        Folder inbox = store.getFolder( "INBOX" );
        inbox.open( Folder.READ_ONLY );
        // Fetch unseen messages from inbox folder
        Message[] messages = inbox.search(
                new FlagTerm(new Flags(Flags.Flag.SEEN), false));

        // Sort messages from recent to oldest
        Arrays.sort( messages, (m1, m2 ) -> {
            try {
                return m2.getSentDate().compareTo( m1.getSentDate() );
            } catch ( MessagingException e ) {
                throw new RuntimeException( e );
            }
        } );

        for ( Message message : messages ) {
            //System.out.println("sendDate: " + message.getSentDate()+ " subject:" + message.getSubject() );
            Object body = message.getContent();
            if(body instanceof String){
                //System.out.println(message.getSubject()); DEBUG
                if(message.getSubject().contains("MTFMSG")) {
                    System.out.println(message.getSubject().replaceAll("MTFMSG:",""));
                    System.out.println(message.getContent());
                    //lastMsg= (String) message.getContent();
                    //System.out.println(lastMsg);
                }
            }
        }
    }
}
